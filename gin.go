package main

import (
	"log"
	"pustaka-api/book"
	//"pustaka-api/handler"
	//"encoding/json"
	//"net/http"
	//"fmt"

	"github.com/gin-gonic/gin"
	//"github.com/go-playground/validator/v10"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"pustaka-api/handler"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(&book.Book{})

	bookRepository := book.NewRepository(db)
	//bookFileRepository := book.NewFileRepository()
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	// bookRequest := book.BookRequest{
	// 	Title:       "Gundam",
	// 	//Description: "Good book",
	// 	Price:       "200000",
	// 	// Rating:      4,
	// 	// Discount:    0,
	// }
	// bookService.Create(bookRequest)

	//fmt.Println("Title :", book.Title)

	// // for _, book := range books {
	// // 	fmt.Println("Titile :", book.Title)
	// }

	//CRUD

	// book := book.Book{}
	// book.Title = "Dunia Shopee"
	// book.Price = 120000
	// book.Discount = 15
	// book.Rating = 5
	// book.Descrition = "Ini adalah buku tentang shopee yang kepo tentang buah apel"

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("=====================")
	// 	fmt.Println("Error creating book record")
	// }

	// var book book.Book

	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil {
	// 	fmt.Println("=====================")
	// 	fmt.Println("Error creating book record")
	// 	fmt.Println("=====================")
	// }

	// err = db.Delete(&book).Error
	// if err != nil {
	// 	fmt.Println("=====================")
	// 	fmt.Println("Error creating book record")
	// 	fmt.Println("=====================")
	// }

	// for _, b := range book {
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Println("book object %v", b)
	// }

	//update data
	// book.Title = "Man Tiger (Revised edition)"
	// err = db.Save(&book).Error
	// if err != nil {
	// 		fmt.Println("=====================")
	// 		fmt.Println("Error creating book record")
	// 		fmt.Println("=====================")
	// }

	router := gin.Default()
	v1 := router.Group("/v1")

	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/hello", bookHandler.HelloHandler)
	// v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	// v1.GET("/query", bookHandler.QueryHandler)
	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
